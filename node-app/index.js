// Simple example getting CIDs of file loaded into the browser 
// "var cids" below is a json object with CIDv0 and CIDv1 for the given file
// Do something other than just log it in production ;) 

import * as wasm from "../pkg/rust_cid_npm";

document.getElementById("input").addEventListener("change", handleFiles, false)

function handleFiles() {

    const reader = new FileReader();
    reader.readAsArrayBuffer(this.files[0]); // MUST read to trigger "onload" defined below

    reader.onload = (event) => {
        var array = new Uint8Array(event.target.result);

        var cids = JSON.parse(wasm.get_cid(array));

        console.log(cids);

        document.getElementById("result").innerHTML = "Input file \"" + this.files[0].name + "\"<br><br>CIDv0: " + cids.CIDv0 +"<br>  CIDv1: " + cids.CIDv1
    }

}

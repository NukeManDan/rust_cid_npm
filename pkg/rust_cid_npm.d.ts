/* tslint:disable */
/* eslint-disable */
/**
* @param {Uint8Array} bytes
* @returns {string}
*/
export function get_cid(bytes: Uint8Array): string;
